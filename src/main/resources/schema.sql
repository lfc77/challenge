create table if not exists carNavigation(
	carID		int(12) primary key,
	latitude	decimal(8,5),
	longitude	decimal(8,5)
);

drop table if exists interestedParties;
 
create table interestedParties(
	placeID		int(12),
	placeName	varchar(100),
	address		varchar(200),
	latitude	decimal(8,5),
	longitude	decimal(8,5)
);

insert into interestedParties(
	placeID,	placeName,	address,		latitude,	longitude
)values(
	1,	'Saks El Original','Av de los Insurgentes Sur 1641 Insurgentes Mixcoac 03900 Ciudad de M�xico, CDMX M�xico',
	19.36296,-99.18271
),
(
	2,	'The Anglo - Anglocentro Florida','Insurgentes Sur 1636 Cr�dito Constructor 03940 Ciudad de M�xico, CDMX M�xico',
	19.36280,-99.18230
);


