package gbm.challenge.gbm.presenters;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="CARNAVIGATION")
public class CarMovementRequest implements Serializable{
	private static final long serialVersionUID = 5308140065849179682L;

	@Id
	private Integer carID;
	
	private String	latitude;
	private String	longitude;
	
	public Integer getCarID() {
		return carID;
	}
	public void setCarID(Integer carID) {
		this.carID = carID;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
