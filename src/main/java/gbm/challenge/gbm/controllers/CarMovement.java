package gbm.challenge.gbm.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import gbm.challenge.gbm.gateway.Places;
import gbm.challenge.gbm.presenters.CarMovementRequest;
import gbm.challenge.gbm.presenters.CarMovementResponse;
import gbm.challenge.gbm.services.RegisterService;

@RestController
@RequestMapping("/car/position")
public class CarMovement {
	@Autowired
	RegisterService registerService;
	
	@RequestMapping(value = "/record", method = RequestMethod.POST)
	public CarMovementResponse registerCar(@RequestBody CarMovementRequest carMovementRequest){
		CarMovementResponse carMovementResponse = registerService.registerCar(carMovementRequest);
		return carMovementResponse;
	}
	
	@RequestMapping(value = "/client", method = RequestMethod.POST)
	public List<Places> getInterestedPlaces(@RequestBody String latitude, @RequestBody String Longitude){
		
		return null;
	}
}
