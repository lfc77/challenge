package gbm.challenge.gbm.gateway;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import gbm.challenge.gbm.presenters.CarMovementRequest;


@Repository
public interface RegisterCarRepository extends JpaRepository<CarMovementRequest, Integer>{
	@Query(nativeQuery = true, value = "insert into CARNAVIGATION values(?,?,?)")
	public void  RegisterCarNav(Integer carID, String latitude, String longitude);
}
